public class TimeModel {
    //private final static String MY_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,-";
    private final static String MY_ALPHABET = "abcd";

    private Value[][] values;


    public TimeModel() {
        int nr = MY_ALPHABET.length();
        values = new Value[nr][nr];
    }

    public void insert(long timeDiff, char a, char b) {
        int idA = getCharIndex(a);
        int idB = getCharIndex(b);

        if (idA > -1 && idB > -1 ) {
            if (values[idA][idB] == null) {
                values[idA][idB] = new Value(timeDiff, a, b);
            } else {
                Value value = values[idA][idB];
                value.refreshAverage(timeDiff);
            }
        }
    }

    public void print() {
        String out = "";
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                if (values[i][j] != null) {
                    out += (values[i][j].getAverage() + "\t");
                } else {
                    out += ("-1\t");
                }
            }
            out += "\n";
        }
        Main.output(out);
    }


    private int getCharIndex(char c) {
        return MY_ALPHABET.indexOf(c);
    }
}

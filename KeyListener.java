import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyListener extends KeyAdapter {

    private TimeModel tm = new TimeModel();
    private char prevChar;
    private long prevTime, currTime;

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getID() == KeyEvent.KEY_PRESSED) {
            if (e.getKeyCode() != KeyEvent.VK_ENTER) {

                if (Character.isDefined(prevChar)) {
                    currTime = System.currentTimeMillis();
                    tm.insert(currTime - prevTime, prevChar, e.getKeyChar());
                }

                prevChar = e.getKeyChar();
                prevTime = System.currentTimeMillis();

            } else {
                tm.print();
                System.exit(0);
            }

        }

    }
}

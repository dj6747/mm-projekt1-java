public class Value {
    private long timeDiff;
    private char a;
    private char b;
    private int nr;
    private double average;

    public Value(long timeDiff, char a, char b) {
        this.timeDiff = timeDiff;
        this.a = a;
        this.b = b;
        this.nr = 1;
        this.average = timeDiff;
    }

    public void refreshAverage(long timeDiff) {
        this.average = ((this.average * this.nr) + timeDiff) / (nr + 1);
        this.nr += 1;
    }

    public double getAverage() {
        return this.average;
    }

}

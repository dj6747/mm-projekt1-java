import javax.swing.*;
import java.awt.*;
import java.io.*;

public class Main {

    private static String outFile = "output/out.txt";
    private static boolean outputToFile = false;

    public static void main(String[] args) {
        if (args.length >= 1) {
            if (args[0].equals("-f")) {
                if (args.length > 1) {
                    outFile = "output/"+args[1];
                }

                outputToFile = true;
            }
        }

        JFrame frame = new JFrame("MM-Projekt1");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dim = new Dimension(400, 400);
        Container c = frame.getContentPane();
        c.setPreferredSize(dim);
        JTextArea ta = new JTextArea();
        ta.setLineWrap(true);
        c.add(ta);
        ta.addKeyListener(new KeyListener());
        frame.pack();
        frame.setVisible(true);
    }

    public static void output(String content) {
        if (outputToFile) {
            BufferedWriter bw = null;
            try {
                File file = new File(outFile);

                if (!file.exists()) {
                    file.createNewFile();
                }

                FileWriter fw = new FileWriter(file);
                bw = new BufferedWriter(fw);
                bw.write(content + "\n");
                bw.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println(content);
        }
    }

}
